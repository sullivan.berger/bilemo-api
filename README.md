# BILEMO API

BILEMO API is a project to achieve in the PHP / Symfony application developer cursus by OpenClassrooms.

## Prerequisites

- PHP >= 8.1
- PostgreSQL
- Composer

## Setup

After cloning the project, you can install dependencies with the following commands :

`composer install`

Once the dependencies are installed, you can run the migration and load the fixtures with the following commands

```
bin/console doctrine:migrations:migrate
bin/console doctrine:fixtures:load
```


[![Codacy Badge](https://app.codacy.com/project/badge/Grade/dbcc81dec7594dadace32c5a29982b71)](https://www.codacy.com/gl/sullivan.berger/bilemo-api/dashboard?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=sullivan.berger/bilemo-api&amp;utm_campaign=Badge_Grade)
