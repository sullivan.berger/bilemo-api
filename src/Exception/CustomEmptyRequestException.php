<?php

namespace App\Exception;

class CustomEmptyRequestException extends \Exception
{
    public function __construct()
    {
        parent::__construct(
            "exceptions.bad_request",
            400
        );
    }
}
