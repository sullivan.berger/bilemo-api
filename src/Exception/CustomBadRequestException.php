<?php

namespace App\Exception;

class CustomBadRequestException extends \Exception
{
    /**
     * @var array
     */
    private array $violations;

    /**
     * @param array<string> $violations
     */
    public function __construct(array $violations)
    {
        $this->violations = $violations;
        parent::__construct(
            $this->getJoinedMessages(),
            400
        );
    }

    /**
     * @return string
     */
    public function getJoinedMessages(): string
    {
        $messages = [];
        foreach ($this->violations as $paramName => $violationList) {
            foreach ($violationList as $violation) {
                $messages[$paramName][] = $violation->getMessage();
            }
            $messages[$paramName] = implode(' ', $messages[$paramName]);
        }
        return $messages[$paramName];
    }
}
