<?php

namespace App\Exception;

class CustomInvalidUuidException extends \Exception
{
    public function __construct()
    {
        parent::__construct(
            "exceptions.invalid_uuid",
            400
        );
    }
}
