<?php

namespace App\Exception;

class CustomNotFoundException extends \Exception
{
    public function __construct()
    {
        parent::__construct(
            "exceptions.not_found",
            404
        );
    }
}
