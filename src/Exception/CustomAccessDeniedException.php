<?php

namespace App\Exception;

class CustomAccessDeniedException extends \Exception
{
    public function __construct()
    {
        parent::__construct(
            "exceptions.access_denied",
            401
        );
    }
}
