<?php

namespace App\Exception;

class CustomForbiddenException extends \Exception
{
    public function __construct()
    {
        parent::__construct(
            "exceptions.forbidden",
            403
        );
    }
}
