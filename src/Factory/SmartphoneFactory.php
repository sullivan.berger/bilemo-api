<?php

namespace App\Factory;

use App\Entity\Smartphone;
use App\Repository\SmartphoneRepository;
use Exception;
use Zenstruck\Foundry\RepositoryProxy;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;

/**
 * @extends ModelFactory<Smartphone>
 *
 * @method static Smartphone|Proxy createOne(array $attributes = [])
 * @method static Smartphone[]|Proxy[] createMany(int $number, array|callable $attributes = [])
 * @method static Smartphone|Proxy find(object|array|mixed $criteria)
 * @method static Smartphone|Proxy findOrCreate(array $attributes)
 * @method static Smartphone|Proxy first(string $sortedField = 'id')
 * @method static Smartphone|Proxy last(string $sortedField = 'id')
 * @method static Smartphone|Proxy random(array $attributes = [])
 * @method static Smartphone|Proxy randomOrCreate(array $attributes = [])
 * @method static Smartphone[]|Proxy[] all()
 * @method static Smartphone[]|Proxy[] findBy(array $attributes)
 * @method static Smartphone[]|Proxy[] randomSet(int $number, array $attributes = [])
 * @method static Smartphone[]|Proxy[] randomRange(int $min, int $max, array $attributes = [])
 * @method static SmartphoneRepository|RepositoryProxy repository()
 * @method Smartphone|Proxy create(array|callable $attributes = [])
 */
final class SmartphoneFactory extends ModelFactory
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return array<string|mixed>
     * @throws Exception
     */
    protected function getDefaults(): array
    {
        return [
            'uuid' => self::faker()->uuid(),
            'brand' => self::faker()->company(),
            'model' => self::faker()->realText(50),
            'color' => self::faker()->colorName(),
            'storage' => self::faker()->randomElement([64, 128, 256, 512]),
            'processor' => self::faker()->realText(20),
            'ram' => self::faker()->randomElement([1024, 2048, 4096, 8192]),
            'cellular' => self::faker()->randomElement(['4G', '4G+', '5G', '5G+']),
            'screenSize' => self::faker()->randomFloat(1, 4, 8),
            'quantity' => self::faker()->randomNumber(),
            'price' => self::faker()->randomFloat(2, 0, 5000),
            'createdAt' => new \DateTimeImmutable(self::faker()->dateTimeBetween('-60 DAYS', '-45 DAYS')->format('Y-m-d H:i:s')),
            'updatedAt' => self::faker()->dateTimeBetween('-30 DAYS', 'NOW')
        ];
    }

    protected function initialize(): self
    {
        // see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#initialization
        return $this
            // ->afterInstantiate(function(Smartphone $smartphone): void {})
        ;
    }

    protected static function getClass(): string
    {
        return Smartphone::class;
    }
}
