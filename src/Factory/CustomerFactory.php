<?php

namespace App\Factory;

use App\Entity\Customer;
use App\Entity\Reseller;
use App\Repository\CustomerRepository;
use App\Repository\ResellerRepository;
use DateTime;
use DateTimeImmutable;
use Exception;
use Zenstruck\Foundry\RepositoryProxy;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;

/**
 * @extends ModelFactory<Customer>
 *
 * @method static Customer|Proxy createOne(array $attributes = [])
 * @method static Customer[]|Proxy[] createMany(int $number, array|callable $attributes = [])
 * @method static Customer|Proxy find(object|array|mixed $criteria)
 * @method static Customer|Proxy findOrCreate(array $attributes)
 * @method static Customer|Proxy first(string $sortedField = 'id')
 * @method static Customer|Proxy last(string $sortedField = 'id')
 * @method static Customer|Proxy random(array $attributes = [])
 * @method static Customer|Proxy randomOrCreate(array $attributes = [])
 * @method static Customer[]|Proxy[] all()
 * @method static Customer[]|Proxy[] findBy(array $attributes)
 * @method static Customer[]|Proxy[] randomSet(int $number, array $attributes = [])
 * @method static Customer[]|Proxy[] randomRange(int $min, int $max, array $attributes = [])
 * @method static CustomerRepository|RepositoryProxy repository()
 * @method Customer|Proxy create(array|callable $attributes = [])
 */
final class CustomerFactory extends ModelFactory
{
    public function __construct()
    {
        parent::__construct();

    }

    /**
     * @return array<Reseller|DateTime|DateTimeImmutable|Proxy|string>
     * @throws Exception
     */
    protected function getDefaults(): array
    {
        return [
            'uuid' => self::faker()->uuid(),
            'firstName' => self::faker()->firstName(),
            'lastName' => self::faker()->lastName(),
            'email' => self::faker()->email(),
            'phoneNumber' => (string) self::faker()->phoneNumber(),
            'address' => self::faker()->streetAddress(),
            'zipCode' => self::faker()->postcode(),
            'city' => self::faker()->city(),
            'country' => self::faker()->country(),
            'reseller' => ResellerFactory::random(),
            'createdAt' => new DateTimeImmutable(self::faker()->dateTimeBetween('-60 DAYS', '-45 DAYS')->format('Y-m-d H:i:s')),
            'updatedAt' => self::faker()->dateTimeBetween('-30 DAYS', 'NOW')
        ];
    }

    protected function initialize(): self
    {
        // see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#initialization
        return $this
            // ->afterInstantiate(function(Customer $customer): void {})
        ;
    }

    protected static function getClass(): string
    {
        return Customer::class;
    }
}
