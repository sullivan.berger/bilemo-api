<?php

namespace App\Entity;

use App\Repository\ResellerRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\Ignore;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

#[ORM\Entity(repositoryClass: ResellerRepository::class)]
#[UniqueEntity(fields: ['email'], message: 'exceptions.email.unique')]
class Reseller implements UserInterface, PasswordAuthenticatedUserInterface
{
    private const USERNAME_REGEX = "/[A-Za-z]/i";
    private const PASSWORD_REGEX = "/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/";

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Ignore]
    private ?int $id = null;

    #[Groups(['customers_read'])]
    #[ORM\Column(type: 'string', length: 180, unique: true)]
    #[Assert\NotBlank(message: 'exceptions.email.blank')]
    #[Assert\Email(message: 'exceptions.email.invalid')]
    private string $email;

    #[ORM\Column(type: 'json')]
    #[Ignore]
    private array $roles = [];

    #[ORM\Column(type: 'string')]
    #[Assert\NotBlank(message: 'exceptions.password.blank')]
    #[Assert\Regex(pattern: self::PASSWORD_REGEX, message: 'exceptions.password.invalid')]
    #[Assert\NotCompromisedPassword(message: 'exceptions.password.compromised')]
    private string $password;

    #[Groups(['customers_read', 'resellers_read'])]
    #[ORM\Column(type: 'string', length: 255)]
    #[Assert\Uuid]
    private string $uuid;

    #[Groups(['customers_read', 'resellers_read'])]
    #[ORM\Column(type: 'string', length: 255)]
    #[Assert\NotBlank(message: 'exceptions.companyName.blank')]
    #[Assert\Type('string')]
    private string $companyName;

    #[Groups(['customers_read', 'resellers_read'])]
    #[ORM\Column(type: 'string', length: 255)]
    #[Assert\NotBlank(message: 'exceptions.firstName.blank')]
    #[Assert\Regex(pattern: self::USERNAME_REGEX, message: 'exceptions.firstName.invalid')]
    private string $firstName;

    #[Groups(['customers_read', 'resellers_read'])]
    #[ORM\Column(type: 'string', length: 255)]
    #[Assert\NotBlank(message: 'exceptions.lastName.blank')]
    #[Assert\Type('string')]
    #[Assert\Regex(pattern: self::USERNAME_REGEX, message: 'exceptions.lastName.invalid')]
    private string $lastName;

    #[ORM\Column(type: 'datetime_immutable')]
    #[Assert\NotNull]
    private \DateTimeImmutable $createdAt;

    #[ORM\Column(type: 'datetime', nullable: true)]
    private ?\DateTime $updatedAt;

    #[ORM\OneToMany(mappedBy: 'reseller', targetEntity: Customer::class, orphanRemoval: true)]
    private Collection $customers;

    public function __construct()
    {
        $this->customers = new ArrayCollection();
        $this->createdAt = new \DateTimeImmutable();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    /**
     * @param array<string> $roles
     * @return $this
     */
    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see PasswordAuthenticatedUserInterface
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials(): void
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getUuid(): ?string
    {
        return $this->uuid;
    }

    public function setUuid(string $uuid): self
    {
        $this->uuid = $uuid;

        return $this;
    }

    public function getCompanyName(): ?string
    {
        return $this->companyName;
    }

    public function setCompanyName(string $companyName): self
    {
        $this->companyName = $companyName;

        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @return Collection<int, Customer>
     */
    public function getCustomers(): Collection
    {
        return $this->customers;
    }

    public function addCustomer(Customer $customer): self
    {
        if (!$this->customers->contains($customer)) {
            $this->customers[] = $customer;
            $customer->setReseller($this);
        }

        return $this;
    }

    public function removeCustomer(Customer $customer): self
    {
        if ($this->customers->removeElement($customer)) {
            // set the owning side to null (unless already changed)
            if ($customer->getReseller() === $this) {
                $customer->setReseller(null);
            }
        }

        return $this;
    }
}
