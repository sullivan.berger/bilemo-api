<?php

namespace App\Entity;

use App\Repository\CustomerRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\Ignore;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

#[ORM\Entity(repositoryClass: CustomerRepository::class)]
#[UniqueEntity(fields: ['email', 'reseller'], message: 'exceptions.customer.unique')]
class Customer
{
    private const USERNAME_REGEX = "/[A-Za-z]/i";

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Ignore]
    private ?int $id = null;

    #[Groups(['customers_read', 'customers_create'])]
    #[ORM\Column(type: 'string', length: 255)]
    #[Assert\Uuid]
    #[Assert\NotNull]
    private string $uuid;

    #[Groups(['customers_read', 'customers_create'])]
    #[ORM\Column(type: 'string', length: 255)]
    #[Assert\NotBlank(message: 'exceptions.firstName.blank')]
    #[Assert\Regex(pattern: self::USERNAME_REGEX, message: 'exceptions.firstName.invalid')]
    private string $firstName;

    #[Groups(['customers_read', 'customers_create'])]
    #[ORM\Column(type: 'string', length: 255)]
    #[Assert\NotBlank(message: 'exceptions.lastName.blank')]
    #[Assert\Regex(pattern: self::USERNAME_REGEX, message: 'exceptions.lastName.invalid')]
    private string $lastName;

    #[Groups(['customers_read', 'customers_create'])]
    #[ORM\Column(type: 'string', length: 255)]
    #[Assert\NotBlank(message: 'exceptions.email.blank')]
    #[Assert\Email(message: 'exceptions.email.invalid')]
    private string $email;

    #[Groups(['customers_read', 'customers_create'])]
    #[ORM\Column(type: 'string', length: 255)]
    #[Assert\Length(
        max: 14,
        minMessage: 'exceptions.phoneNumber.minMessage',
        maxMessage: 'exceptions.phoneNumber.maxMessage'
    )]
    #[Assert\Type('string')]
    private string $phoneNumber;

    #[Groups(['customers_read', 'customers_create'])]
    #[ORM\Column(type: 'string', length: 255)]
    #[Assert\NotBlank(message: 'exceptions.address.blank')]
    #[Assert\Type('string')]
    private string $address;

    #[Groups(['customers_read', 'customers_create'])]
    #[ORM\Column(type: 'string', length: 255)]
    #[Assert\NotBlank(message: 'exceptions.zipCode.blank')]
    #[Assert\Type('string')]
    private string $zipCode;

    #[Groups(['customers_read', 'customers_create'])]
    #[ORM\Column(type: 'string', length: 255)]
    #[Assert\NotBlank(message: 'exceptions.city.blank')]
    #[Assert\Type('string')]
    private string $city;

    #[Groups(['customers_read', 'customers_create'])]
    #[ORM\Column(type: 'string', length: 255)]
    #[Assert\NotBlank(message: 'exceptions.country.blank')]
    #[Assert\Type('string')]
    private string $country;

    #[Groups(['customers_read', 'customers_create'])]
    #[ORM\Column(type: 'datetime_immutable')]
    #[Assert\NotNull]
    private \DateTimeImmutable $createdAt;

    #[Groups(['customers_read', 'customers_create'])]
    #[ORM\Column(type: 'datetime', nullable: true)]
    private ?\DateTime $updatedAt;

    #[Groups(['customers_read'])]
    #[ORM\ManyToOne(targetEntity: Reseller::class, inversedBy: 'customers')]
    #[ORM\JoinColumn(nullable: false)]
    #[Assert\NotNull]
    private Reseller $reseller;

    public function __construct()
    {
        $this->createdAt = new \DateTimeImmutable();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUuid(): ?string
    {
        return $this->uuid;
    }

    public function setUuid(string $uuid): self
    {
        $this->uuid = $uuid;

        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPhoneNumber(): ?string
    {
        return $this->phoneNumber;
    }

    public function setPhoneNumber(string $phoneNumber): self
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getZipCode(): ?string
    {
        return $this->zipCode;
    }

    public function setZipCode(string $zipCode): self
    {
        $this->zipCode = $zipCode;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function setCountry(string $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getReseller(): ?Reseller
    {
        return $this->reseller;
    }

    public function setReseller(Reseller|UserInterface|null $reseller): self
    {
        $this->reseller = $reseller;

        return $this;
    }
}
