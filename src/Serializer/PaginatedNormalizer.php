<?php

namespace App\Serializer;

use App\Entity\Customer;
use App\Entity\Smartphone;
use App\Service\Pagination\PaginationService;
use ArrayObject;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\CacheableSupportsMethodInterface;
use Symfony\Component\Serializer\Normalizer\ContextAwareNormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class PaginatedNormalizer implements ContextAwareNormalizerInterface, CacheableSupportsMethodInterface
{
    private UrlGeneratorInterface $router;
    private PaginationService $paginationService;
    private ObjectNormalizer $normalizer;
    private RequestStack $requestStack;
    private Security $security;

    public function __construct(
        UrlGeneratorInterface $router,
        PaginationService $paginationService,
        ObjectNormalizer $normalizer,
        RequestStack $requestStack,
        Security $security
    ) {
        $this->router = $router;
        $this->paginationService = $paginationService;
        $this->normalizer = $normalizer;
        $this->requestStack = $requestStack;
        $this->security = $security;
    }

    /**
     * @param mixed $collection
     * @param string|null $format
     * @param array<string> $context
     * @return array<string>
     * @throws ExceptionInterface
     */
    public function normalize(
        mixed $collection,
        string $format = null,
        array $context = []
    ): array
    {
        $data = $this->normalizer->normalize($collection, $format, $context);
        unset($data['limit'], $data['page']);

        $route = $this->requestStack->getCurrentRequest()->attributes->get('_route');

        $baseUrl = "";

        if ($route === 'read_paginated_customers') {
            $reseller = $this->security->getUser();
            $baseUrl = $this->router->generate('read_paginated_customers', [], UrlGeneratorInterface::ABSOLUTE_URL);
            $pagination['_pagination']['total_items'] = $this->paginationService->getCount(Customer::class, ["reseller" => $reseller]);
            if ($this->paginationService->getPage() >= $this->paginationService->getLastPage(Customer::class, ["reseller" => $reseller])) {
                $pagination['_pagination']['next_page_link'] = sprintf($baseUrl . '?page=%s', $this->paginationService->getLastPage(Customer::class, ["reseller" => $reseller]));
            } else {
                $pagination['_pagination']['next_page_link'] = sprintf($baseUrl . '?page=%s', $this->paginationService->getPage() + 1);
            }
            $pagination['_pagination']['last_page_link'] = sprintf($baseUrl . '?page=%s', $this->paginationService->getLastPage(Customer::class, ["reseller" => $reseller]));
        }

        if ($route === 'read_paginated_smartphones') {
            $baseUrl = $this->router->generate('read_paginated_smartphones', [], UrlGeneratorInterface::ABSOLUTE_URL);
            $pagination['_pagination']['total_items'] = $this->paginationService->getCount(Smartphone::class, []);
            if ($this->paginationService->getPage() >= $this->paginationService->getLastPage(Smartphone::class, [])) {
                $pagination['_pagination']['next_page_link'] = sprintf($baseUrl . '?page=%s', $this->paginationService->getLastPage(Smartphone::class, []));
            } else {
                $pagination['_pagination']['next_page_link'] = sprintf($baseUrl . '?page=%s', $this->paginationService->getPage() + 1);
            }
            $pagination['_pagination']['last_page_link'] = sprintf($baseUrl . '?page=%s', $this->paginationService->getLastPage(Smartphone::class, []));

        }

        $pagination['_pagination']['current_page'] = $this->paginationService->getPage();
        $pagination['_pagination']['items_per_page'] = $this->paginationService->getLimit();
        if ($this->paginationService->getPage() < 2) {
            $pagination['_pagination']['previous_page_link'] = $baseUrl;
        } else {
            $pagination['_pagination']['previous_page_link'] = sprintf($baseUrl . '?page=%s', $this->paginationService->getPage() - 1);
        }
        $pagination['_pagination']['first_page_link'] = $baseUrl;

        return array_merge($pagination, $data);
    }

    /**
     * @param mixed $data
     * @param string|null $format
     * @param array<string> $context
     * @return bool
     */
    public function supportsNormalization(mixed $data, string $format = null, array $context = []): bool
    {
        return $data instanceof PaginationService;
    }

    public function hasCacheableSupportsMethod(): bool
    {
        return true;
    }
}
