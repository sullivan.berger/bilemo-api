<?php

namespace App\Serializer;

use App\Entity\Smartphone;
use ArrayObject;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\CacheableSupportsMethodInterface;
use Symfony\Component\Serializer\Normalizer\ContextAwareNormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class SmartphoneNormalizer implements ContextAwareNormalizerInterface, CacheableSupportsMethodInterface
{
    private UrlGeneratorInterface $router;
    private ObjectNormalizer $normalizer;

    public function __construct(
        UrlGeneratorInterface $router,
        ObjectNormalizer $normalizer
    )
    {
        $this->router = $router;
        $this->normalizer = $normalizer;
    }

    /**
     * @param mixed $smartphone
     * @param string|null $format
     * @param array<string> $context
     * @return array|ArrayObject|bool|float|int|mixed|string|null
     * @throws ExceptionInterface
     */
    public function normalize(mixed $smartphone, string $format = null, array $context = [])
    {
        $data = $this->normalizer->normalize($smartphone, $format, $context);

        $data["_link"]["self"] = $this->router->generate('read_smartphone_by_uuid', [
            "uuid" => $smartphone->getUuid(), UrlGeneratorInterface::ABSOLUTE_URL
        ]);

        return $data;
    }

    /**
     * @param mixed $data
     * @param string|null $format
     * @param array<string> $context
     * @return bool
     */
    public function supportsNormalization(mixed $data, string $format = null, array $context = []): bool
    {
        return $data instanceof Smartphone;
    }

    public function hasCacheableSupportsMethod(): bool
    {
        return true;
    }
}
