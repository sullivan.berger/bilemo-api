<?php

namespace App\Serializer;

use App\Entity\Customer;
use ArrayObject;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\CacheableSupportsMethodInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class CustomerNormalizer implements NormalizerInterface, CacheableSupportsMethodInterface
{
    private UrlGeneratorInterface $router;
    private ObjectNormalizer $normalizer;

    public function __construct(
        UrlGeneratorInterface $router,
        ObjectNormalizer$normalizer,
    )
    {
        $this->router = $router;
        $this->normalizer = $normalizer;
    }

    /**
     * @param mixed $customer
     * @param string|null $format
     * @param array<string> $context
     * @return array|ArrayObject|bool|float|int|mixed|string|null
     * @throws ExceptionInterface
     */
    public function normalize(mixed $customer, string $format = null, array $context = [])
    {
        $data = $this->normalizer->normalize($customer, $format, $context);

        $data['_links']['self'] = $this->router->generate('read_customer_by_uuid', [
            'uuid' => $customer->getUuid(),
        ], UrlGeneratorInterface::ABSOLUTE_URL);

        $data['_links']['create'] = $this->router->generate('create_customer', [], UrlGeneratorInterface::ABSOLUTE_URL);

        $data['_links']['delete'] = $this->router->generate('delete_customer_by_uuid', [
            'uuid' => $customer->getUuid(),
        ], UrlGeneratorInterface::ABSOLUTE_URL);

        return $data;

    }

    /**
     * @param mixed $data
     * @param string|null $format
     * @return bool
     */
    public function supportsNormalization(mixed $data, string $format = null)
    {
        return $data instanceof Customer;
    }

    public function hasCacheableSupportsMethod(): bool
    {
        return true;
    }
}
