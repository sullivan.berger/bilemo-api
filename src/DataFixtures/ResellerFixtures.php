<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\Factory\ResellerFactory;

class ResellerFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        ResellerFactory::createMany(12);
    }
}
