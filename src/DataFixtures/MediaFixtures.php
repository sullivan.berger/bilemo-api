<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use App\Factory\MediaFactory;

class MediaFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        MediaFactory::createMany(500);
    }

    public function getDependencies(): array
    {
        return [
            SmartphoneFixtures::class
        ];
    }
}
