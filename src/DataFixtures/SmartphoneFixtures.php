<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\Factory\SmartphoneFactory;

class SmartphoneFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        SmartphoneFactory::createMany(80);
    }
}
