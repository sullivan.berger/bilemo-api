<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use App\Factory\CustomerFactory;

class CustomerFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        CustomerFactory::createMany(200);
    }

    public function getDependencies(): array
    {
        return [
            ResellerFixtures::class
        ];
    }
}
