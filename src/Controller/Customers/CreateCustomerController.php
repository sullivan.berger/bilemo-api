<?php

namespace App\Controller\Customers;

use App\Entity\Customer;
use App\Exception\CustomBadRequestException;
use App\Exception\CustomEmptyRequestException;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Cache\InvalidArgumentException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\DecoderInterface;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Cache\TagAwareCacheInterface;
use OpenApi\Attributes as OA;
use Nelmio\ApiDocBundle\Annotation\Operation;

class CreateCustomerController extends AbstractController
{
    #[Route('/api/v1/customers', name: 'create_customer', methods: ['POST'])]
    #[Operation([
        "summary" => "Create a new Customer",
        "tags" => [
            "Customers"
        ]
    ])]
    #[OA\RequestBody(
        description: "Input data format",
        content: new OA\JsonContent(
            example: [
                "firstName" => "John",
                "lastName" => "Doe",
                "email" => "john@doe.com",
                "phoneNumber" => "0606060606",
                "address" => "10 rue de la Paix",
                "zipCode" => "34000",
                "city" => "Montpellier",
                "country" => "France"
            ]
        )
    )]
    #[OA\Response(
        response: 201,
        description: "Created",
        content: new OA\JsonContent(
            example: [
                "uuid" => "4497649d-37d3-4e9f-aab8-c9e586e09434",
                "firstName" => "John",
                "lastName" => "Doe",
                "email" => "john@doe.com",
                "phoneNumber" => "0606060606",
                "address" => "10 rue de la Paix",
                "zipCode" => "34000",
                "city" => "Montpellier",
                "country" => "France",
                "createdAt" => "2022-06-26T10:11:17+00:00",
                "updatedAt" => null,
            ]
        )
    )]

    #[OA\Response(
        response: 400,
        description: "Bad Request"
    )]
    /**
     * @throws InvalidArgumentException
     * @throws CustomEmptyRequestException
     * @throws CustomBadRequestException
     */
    public function __invoke(
        Request $request,
        EntityManagerInterface $entityManager,
        ValidatorInterface $validator,
        SerializerInterface $serializer,
        TagAwareCacheInterface $cache,
        DecoderInterface $decoder
    ): Response
    {
        if (!empty($decoder->decode($request->getContent(), JsonEncoder::FORMAT))) {

            /** @var Customer $customer */
            $customer = $serializer->deserialize($request->getContent(), Customer::class, 'json');
            $reseller = $this->getUser();
            $customer
                ->setUuid(Uuid::v4())
                ->setReseller($reseller)
            ;

            $violations = $validator->validate($customer);

            if (count($violations) > 0) {
                throw new CustomBadRequestException((array) $violations);
            }

            $entityManager->persist($customer);
            $entityManager->flush();

            $cache->invalidateTags(["customers"]);

            return $this->json($customer, 201, [], [
                "groups" => ["customers_create"]
            ]);
        }

        throw new CustomEmptyRequestException();
    }
}
