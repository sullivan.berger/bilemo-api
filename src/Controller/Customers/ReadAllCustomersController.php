<?php

namespace App\Controller\Customers;

use App\Entity\Customer;
use App\Service\Pagination\PaginationService;
use Nelmio\ApiDocBundle\Annotation\Operation;
use Psr\Cache\InvalidArgumentException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Contracts\Cache\ItemInterface;
use Symfony\Contracts\Cache\TagAwareCacheInterface;
use OpenApi\Attributes as OA;

class ReadAllCustomersController extends AbstractController
{
    #[Route('/api/v1/customers', name: 'read_paginated_customers', methods: ['GET'])]
    #[Operation([
        "summary" => "Fetch a paginated collection of customers",
        "tags" => ["Customers"]
    ])]
    #[OA\Parameter(
        name: "page",
        description: "Page to request",
        in: "query",
        required: false,
        schema: new OA\Schema(type: "integer")
    )]
    #[OA\Response(
        response: 200,
        description: "Ok",
        content: new OA\JsonContent(
            example: [
                [
                    "uuid" => "4497649d-37d3-4e9f-aab8-c9e586e09434",
                    "firstName" => "John",
                    "lastName" => "Doe",
                    "email" => "john@doe.com",
                    "phoneNumber" => "0606060606",
                    "address" => "10 rue de la Paix",
                    "zipCode" => 34000,
                    "city" => "Montpellier",
                    "country" => "France",
                    "createdAt" => "2022-06-26T10:11:17+00:00",
                    "updatedAt" => null,
                ],
                [
                    "uuid" => "4497649d-37d3-4e9f-aab8-c9e586e07cbe",
                    "firstName" => "Jane",
                    "lastName" => "Doe",
                    "email" => "jane@doe.com",
                    "phoneNumber" => "0606060607",
                    "address" => "12 rue de la Paix",
                    "zipCode" => 34000,
                    "city" => "Montpellier",
                    "country" => "France",
                    "createdAt" => "2022-06-26T10:11:17+00:00",
                    "updatedAt" => null,
                ]
            ]
        )
    )]
    #[OA\Response(
        response: 401,
        description: "Access Denied"
    )]
    /**
     * @throws InvalidArgumentException
     */
    public function __invoke(
        PaginationService $paginationService,
        TagAwareCacheInterface $cache
    ): JsonResponse
    {

        return $cache->get("customers_" . $paginationService->getPage(), function (ItemInterface $item) use ($paginationService) {

            $item->expiresAfter(3600);
            $item->tag(["customers"]);
            $this->denyAccessUnlessGranted('read_all_customer', $this->getUser());

            $customers = $paginationService->createPagination(Customer::class,['reseller' => $this->getUser()],['createdAt' => 'DESC']);

            return $this->json($customers, 200, [], [
                AbstractNormalizer::IGNORED_ATTRIBUTES => ['reseller']
            ]);
        });
    }
}
