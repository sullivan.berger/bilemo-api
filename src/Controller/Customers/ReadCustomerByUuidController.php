<?php

namespace App\Controller\Customers;

use App\Exception\CustomNotFoundException;
use App\Repository\CustomerRepository;
use Nelmio\ApiDocBundle\Annotation\Operation;
use Psr\Cache\InvalidArgumentException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Attributes as OA;
use Symfony\Component\Uid\Uuid;
use Symfony\Contracts\Cache\ItemInterface;
use Symfony\Contracts\Cache\TagAwareCacheInterface;


class ReadCustomerByUuidController extends AbstractController
{
    #[Route('/api/v1/customers/{uuid}', name: 'read_customer_by_uuid', methods: ['GET'])]
    #[Operation([
        'summary' => "Fetch a customer resource by a given customer uuid",
        'tags' => ['Customers']
    ])]
    #[OA\Parameter(
        name: "uuid",
        in: "path",
        required: true,
        schema: new OA\Schema(type: "string")
    )]
    #[OA\Response(
        response: 200,
        description: "Ok",
        content: new OA\JsonContent(
            example: [
                "uuid" => "4497649d-37d3-4e9f-aab8-c9e586e09434",
                "firstName" => "John",
                "lastName" => "Doe",
                "email" => "john@doe.com",
                "phoneNumber" => "0606060606",
                "address" => "10 rue de la Paix",
                "zipCode" => 34000,
                "city" => "Montpellier",
                "country" => "France",
                "createdAt" => "2022-06-26T10:11:17+00:00",
                "updatedAt" => null,
                "reseller" => [
                    [
                        "email"=> "jane@gmail.com",
                        "uuid" => "58c82cb1-2cbf-3792-8fa3-8988638ac24e",
                        "companyName" => "Tata Inc.",
                        "firstName" => "Jane",
                        "lastName" => "Doe"
                    ]
                ]
            ]
        )
    )]
    #[OA\Response(
        response: 404,
        description: "Not Found"
    )]
    /**
     * @throws InvalidArgumentException
     */
    public function __invoke(
        Uuid $uuid,
        CustomerRepository $customerRepository,
        TagAwareCacheInterface $cache,
    ): Response
    {

        return $cache->get("customers_" . $uuid, function (ItemInterface $item) use ($uuid, $customerRepository) {

            $item->expiresAfter(3600);
            $item->tag(["customers"]);

            $customer = $customerRepository->findOneBy(["uuid" => $uuid]);
            $reseller = $this->getUser();

            if ($customer === null) {
                throw new CustomNotFoundException();
            }

            if ($customer->getReseller()->getEmail() !== $reseller->getUserIdentifier()) {
                throw new CustomNotFoundException();
            }

            return $this->json($customer, 200, [], [
                "groups" => ["customers_read",]
            ]);
        });
    }
}
