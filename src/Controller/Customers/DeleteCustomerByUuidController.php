<?php

namespace App\Controller\Customers;

use App\Entity\Customer;
use App\Exception\CustomAccessDeniedException;
use App\Exception\CustomNotFoundException;
use App\Repository\CustomerRepository;
use App\Repository\ResellerRepository;
use Doctrine\ORM\EntityManagerInterface;
use Nelmio\ApiDocBundle\Annotation\Operation;
use Psr\Cache\InvalidArgumentException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Attributes as OA;
use Symfony\Component\Uid\Uuid;
use Symfony\Contracts\Cache\TagAwareCacheInterface;

class DeleteCustomerByUuidController extends AbstractController
{
    #[Route('/api/v1/customers/{uuid}', name: 'delete_customer_by_uuid', methods: ['DELETE'])]
    #[Operation([
        'summary' => "Delete customer by a given customer uuid",
        'tags' => ['Customers']
    ])]
    #[OA\Parameter(
        name: "uuid",
        in: "path",
        required: true,
        schema: new OA\Schema(type: "string")
    )]
    #[OA\Response(
        response: 204,
        description: "No Content"
    )]
    #[OA\Response(
        response: 404,
        description: "Not Found"
    )]
    /**
     * @throws CustomNotFoundException
     * @throws InvalidArgumentException
     */
    public function __invoke(
        Uuid $uuid,
        CustomerRepository $customerRepository,
        ResellerRepository $resellerRepository,
        EntityManagerInterface $entityManager,
        TagAwareCacheInterface $cache
    ): Response
    {
        $customer = $customerRepository->findOneBy(["uuid" => $uuid]);
        $reseller = $resellerRepository->findOneBy(["email" => $this->getUser()->getUserIdentifier()]);

        if (null !== $customer && $customer->getReseller() === $reseller) {

            $entityManager->remove($customer);
            $entityManager->flush();

            $cache->invalidateTags(["customers"]);

            return $this->json(null, 204, [], []);
        }
        throw new CustomNotFoundException();
    }
}
