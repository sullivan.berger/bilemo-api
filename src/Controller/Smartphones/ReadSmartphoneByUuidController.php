<?php

namespace App\Controller\Smartphones;

use App\Exception\CustomNotFoundException;
use App\Repository\SmartphoneRepository;
use Nelmio\ApiDocBundle\Annotation\Operation;
use Psr\Cache\InvalidArgumentException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Attributes as OA;
use Symfony\Component\Uid\Uuid;
use Symfony\Contracts\Cache\ItemInterface;
use Symfony\Contracts\Cache\TagAwareCacheInterface;

class ReadSmartphoneByUuidController extends AbstractController
{
    #[Route('/api/v1/smartphones/{uuid}', name: 'read_smartphone_by_uuid', methods: ['GET'])]
    #[Operation([
        'summary' => "Fetch a smartphone resource by a given uuid.",
        'tags' => ['Smartphones']
    ])]
    #[OA\Parameter(
        name: "uuid",
        in: "path",
        required: true,
        schema: new OA\Schema(type: "string")
    )]
    #[OA\Response(
        response: 200,
        description: "Ok",
        content: new OA\JsonContent(
            example: [
                "uuid" => "6428eca2-1fec-354b-97cb-eb0ed4321115",
                "brand" => "Pfannerstill-Price",
                "model" => "I was, I shouldn't like THAT!' 'Oh, you can't.",
                "color" => "LightSlateGray",
                "storage" => 128,
                "processor" => "Alice to herself.",
                "ram" => 1024,
                "cellular" => "5G+",
                "screenSize" => 5.5,
                "quantity" => 33956804,
                "createdAt" => "2022-03-20T10:12:02+00:00",
                "updatedAt" => "2022-04-23T19:15:30+00:00",
                "price" => 2710.09,
                "medias" => [
                    [
                        "uuid" => "1dbba9c8-76b7-3b11-b750-fbd56aac8dde",
                        "type" => "image",
                        "url" => "http://ortiz.com/aspernatur-impedit-ex-qui-nemo-iste-non-cumque-quaerat",
                        "createdAt" => "2022-05-10T13:01:07+00:00",
                        "updatedAt" => null
                    ]
                ]
            ]
        )
    )]
    #[OA\Response(
        response: 404,
        description: "Not Found"
    )]
    /**
     * @throws InvalidArgumentException
     */
    public function __invoke(
        Uuid $uuid,
        SmartphoneRepository $smartphoneRepository,
        TagAwareCacheInterface $cache
    ): Response
    {

        return $cache->get('smartphone_' . $uuid, function (ItemInterface $item) use ($smartphoneRepository, $uuid) {

            $item->expiresAfter(3600);
            $item->tag(["smartphones"]);
            $smartphone = $smartphoneRepository->findOneBy(["uuid" => $uuid]);

            if ($smartphone === null) {
                throw new CustomNotFoundException();
            }

            return $this->json($smartphone, 200, [], [
                "groups" => "smartphones_read"
            ]);
        });
    }
}
