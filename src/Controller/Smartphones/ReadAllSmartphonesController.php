<?php

namespace App\Controller\Smartphones;

use App\Entity\Smartphone;
use App\Service\Pagination\PaginationService;
use Nelmio\ApiDocBundle\Annotation\Operation;
use Psr\Cache\InvalidArgumentException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Contracts\Cache\ItemInterface;
use Symfony\Contracts\Cache\TagAwareCacheInterface;
use OpenApi\Attributes as OA;


class ReadAllSmartphonesController extends AbstractController
{
    #[Route('/api/v1/smartphones', name: 'read_paginated_smartphones', methods: ['GET'])]
    #[Operation([
        'summary' => "Fetch a paginated collection of smartphones",
        'tags' => ['Smartphones']
    ])]
    #[OA\Parameter(
        name: "page",
        description: "Page to request",
        in: "query",
        required: false,
        schema: new OA\Schema(type: "integer")
    )]
    #[OA\Response(
        response: 200,
        description: "Ok",
        content: new OA\JsonContent(
            example: [
                [
                    "uuid" => "279714c8-84b5-34f7-8b6f-8f720167f7d6",
                    "brand" => "Homenick-Beahan",
                    "model" => "I'd only been the right size for going through.",
                    "color" => "PapayaWhip",
                    "storage" => 256,
                    "processor" => "Hatter, 'or you'll.",
                    "ram" => 2048,
                    "cellular" => "4G",
                    "screenSize" => 5.2,
                    "quantity" => 4802348,
                    "createdAt" => "2022-03-20T12:12:08+00:00",
                    "updatedAt" => "2022-05-04T22:02:50+00:00",
                    "price" => 4900.47
                ],
                [
                    "uuid" => "6428eca2-1fec-354b-97cb-eb0ed4321115",
                    "brand" => "Pfannerstill-Price",
                    "model" => "I was, I shouldn't like THAT!' 'Oh, you can't.",
                    "color" => "LightSlateGray",
                    "storage" => 128,
                    "processor" => "Alice to herself.",
                    "ram" => 1024,
                    "cellular" => "5G+",
                    "screenSize" => 5.5,
                    "quantity" => 33956804,
                    "createdAt" => "2022-03-20T10:12:02+00:00",
                    "updatedAt" => "2022-04-23T19:15:30+00:00",
                    "price" => 2710.09
                ]
            ]
        )
    )]
    #[IsGranted('ROLE_USER')]
    /**
     * @throws InvalidArgumentException
     */
    public function __invoke(
        PaginationService $paginationService,
        TagAwareCacheInterface $cache
    ): Response
    {

        return $cache->get('smartphones_page' . $paginationService->getPage(), function (ItemInterface $item) use ($paginationService) {

            $item->expiresAfter(3600);
            $item->tag(["smartphones"]);

            $smartphones = $paginationService->createPagination(Smartphone::class, [], ['createdAt' => 'DESC']);

            return $this->json($smartphones, 200, [], [
                AbstractNormalizer::IGNORED_ATTRIBUTES => ['medias']
            ]);
        });



    }
}
