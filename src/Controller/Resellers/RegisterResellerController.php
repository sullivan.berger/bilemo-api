<?php

namespace App\Controller\Resellers;

use App\Entity\Reseller;
use App\Exception\CustomBadRequestException;
use App\Exception\CustomEmptyRequestException;
use Doctrine\ORM\EntityManagerInterface;
use Nelmio\ApiDocBundle\Annotation\Operation;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\DecoderInterface;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Uid\Uuid;
use OpenApi\Attributes as OA;

class RegisterResellerController extends AbstractController
{
    #[Route('/api/v1/resellers', name: 'register_reseller', methods: ['POST'])]
    #[Operation([
        'summary' => "Register a new reseller",
        'tags' => ['Resellers']
    ])]
    #[OA\RequestBody(
        description: "Input data format",
        content: new OA\JsonContent(
            example: [
                "firstName" => "John",
                "lastName" => "Doe",
                "email" => "john@doe.com",
                "companyName" => "Toto Inc.",
                "password" => "P@Ssw0rd!!"
            ]
        )
    )]
    #[OA\Response(
        response: 201,
        description: "Created",
        content: new OA\JsonContent(
            example: [
                "uuid" => "7d420824-3781-4a19-af4d-b636e60a787f",
                "companyName" => "Toto Inc.",
                "firstName" => "John",
                "lastName" => "Doe"
            ]
        )
    )]
    #[OA\Response(
        response: 400,
        description: "Bad Request"
    )]
    /**
     * @throws CustomBadRequestException
     * @throws CustomEmptyRequestException
     */
    public function __invoke(
        Request $request,
        SerializerInterface $serializer,
        ValidatorInterface $validator,
        EntityManagerInterface $entityManager,
        UserPasswordHasherInterface $passwordHasher,
        DecoderInterface $decoder
    ): JsonResponse
    {

        if (!empty($decoder->decode($request->getContent(), JsonEncoder::FORMAT))) {
            /** @var Reseller $reseller */
            $reseller = $serializer->deserialize($request->getContent(), Reseller::class, 'json');

            $violations = $validator->validate($reseller);

            $reseller->setUuid(Uuid::v4());

            if (count($violations) > 0) {
                throw new CustomBadRequestException((array) $violations);
            }

            $reseller
                ->setPassword($passwordHasher->hashPassword($reseller, $reseller->getPassword()))
            ;

            $entityManager->persist($reseller);
            $entityManager->flush();

            return $this->json($reseller, 201, [], [
                "groups" => ["resellers_read"]
            ]);
        }

        throw new CustomEmptyRequestException();
    }
}
