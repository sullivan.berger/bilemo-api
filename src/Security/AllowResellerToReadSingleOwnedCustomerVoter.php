<?php

namespace App\Security;

use App\Entity\Customer;
use App\Entity\Reseller;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;

class AllowResellerToReadSingleOwnedCustomerVoter extends Voter
{
    private const ALLOWED = 'read_single_customer';

    protected function supports(string $attribute, mixed $subject): bool
    {
        if ($attribute != self::ALLOWED) {
            return false;
        }

        if (!$subject instanceof Customer) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute(string $attribute, mixed $subject, TokenInterface $token): bool
    {

        $user = $token->getUser();

        if (!$user instanceof Reseller) {
            return false;
        }

        $customer = $subject;

        return $this->canRead($customer, $user);

    }

    private function canRead(Customer $customer, UserInterface $user): bool
    {
        return ($user === $customer->getReseller());
    }
}
