<?php

namespace App\Security;

use App\Entity\Reseller;
use App\Repository\ResellerRepository;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;

class AllowResellerToReadAllOwnedCustomerVoter extends Voter
{
    private const ALLOWED = 'read_all_customer';
    private ResellerRepository $resellerRepository;

    public function __construct(
        ResellerRepository $resellerRepository
    )
    {
        $this->resellerRepository = $resellerRepository;
    }

    protected function supports(string $attribute, mixed $subject): bool
    {
        if ($attribute != self::ALLOWED) {
            return false;
        }

        if (!$subject instanceof Reseller) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute(string $attribute, mixed $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();
        $reseller = $this->resellerRepository->findOneBy(["email" => $user->getUserIdentifier()]);

        if (!$reseller instanceof Reseller) {
            return false;
        }

        $reseller = $subject;

        return $this->canRead($reseller, $user);
    }

    private function canRead(Reseller $reseller, UserInterface $user): bool
    {
        $userUuid = $this->resellerRepository->findOneBy(["email" => $user->getUserIdentifier()])->getUuid();

        return ($reseller->getUuid() === $userUuid);
    }
}
