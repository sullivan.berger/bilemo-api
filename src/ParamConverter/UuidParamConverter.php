<?php

namespace App\ParamConverter;

use App\Exception\CustomInvalidUuidException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Uid\Uuid;

class UuidParamConverter implements ParamConverterInterface
{
    /**
     * @throws CustomInvalidUuidException
     */
    public function apply(Request $request, ParamConverter $configuration): bool
    {

        $param = $configuration->getName();

        if (!$request->attributes->has($param)) {
            return false;
        }

        $value = $request->attributes->get($param);

        if ($value === "" && $configuration->isOptional()) {
            $request->attributes->set($param, null);
            return true;
        }

        if (!Uuid::isValid($value)) {
            throw new CustomInvalidUuidException();
        }

        $data = Uuid::fromString($value);

        $request->attributes->set($param, $data);

        return true;
    }

    public function supports(ParamConverter $configuration): bool
    {
        $className = $configuration->getClass();

        return $className !== "" && $className === Uuid::class;

    }
}
