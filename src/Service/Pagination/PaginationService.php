<?php

namespace App\Service\Pagination;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\User\UserInterface;

class PaginationService
{
    private EntityManagerInterface $em;
    private RequestStack $requestStack;
    private ParameterBagInterface $params;
    private int $page;
    private mixed $data;

    public function __construct(
        EntityManagerInterface $em,
        RequestStack $requestStack,
        ParameterBagInterface $params
    )
    {
        $this->em = $em;
        $this->requestStack = $requestStack;
        $this->params = $params;
        $currentRequest = $this->requestStack->getCurrentRequest();
        $this->page = $currentRequest->query->getInt('page', 1);
    }

    /**
     * @param class-string $repository
     * @param array<string|UserInterface|null> $criteria
     * @param array<string> $orderBy
     * @return PaginationService
     */
    public function createPagination(string $repository, array $criteria, array $orderBy): self
    {

        if ($this->page < 1) {
            $this->page = 1;
        }

        $limit = $this->getLimit();
        $count = $this->getCount($repository, []);

        $offset = ($this->page * $limit - $limit);

        if (($limit + $offset) > $count) {
            $offset = ($count / $limit) - $limit;
        }

        $this->data = $this->em->getRepository($repository)->findBy($criteria, $orderBy, $limit, $offset);

        return $this;
    }

    /**
     * @return mixed
     */
    public function getData(): mixed
    {
        return $this->data;
    }

    /**
     * @return int
     */
    public function getLimit(): int
    {
        return (int) $this->params->get('app.pagination.limit');
    }

    /**
     * @template T of object
     * @param class-string<T> $repository
     * @param array<string|UserInterface|null> $criteria
     * @return int
     */
    public function getCount(string $repository, array $criteria): int
    {
        return $this->em->getRepository($repository)->count($criteria);
    }

    /**
     * @return int
     */
    public function getPage(): int
    {
        return $this->page;
    }

    /**
     * @param string $repository
     * @param array<string|UserInterface|null> $criteria
     * @return int
     */
    public function getLastPage(string $repository, array $criteria): int
    {
        return (int) round($this->getCount($repository, $criteria) / $this->getLimit());
    }
}
