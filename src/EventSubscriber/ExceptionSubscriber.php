<?php

namespace App\EventSubscriber;

use App\Exception\CustomAccessDeniedException;
use App\Exception\CustomBadRequestException;
use App\Exception\CustomEmptyRequestException;
use App\Exception\CustomForbiddenException;
use App\Exception\CustomInvalidUuidException;
use App\Exception\CustomNotFoundException;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Contracts\Translation\TranslatorInterface;

class ExceptionSubscriber implements EventSubscriberInterface
{
    private TranslatorInterface $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::EXCEPTION => [
                ['processException', 10],
            ],
        ];
    }

    public function processException(ExceptionEvent $event): void
    {
        $throwable = $event->getThrowable();

        $response = match (true) {
            $throwable instanceof CustomBadRequestException,
            $throwable instanceof CustomInvalidUuidException,
            $throwable instanceof CustomNotFoundException,
            $throwable instanceof CustomEmptyRequestException,
            $throwable instanceof  CustomForbiddenException,
            $throwable instanceof CustomAccessDeniedException => [
                'message' => $this->translator->trans($throwable->getMessage()),
                'status_code' => $throwable->getCode()
            ],
            $throwable instanceof BadRequestHttpException => [
                'message' => $throwable->getMessage(),
                'status_code' => 400
            ],
            $throwable instanceof NotFoundHttpException => [
                'message' => $throwable->getMessage(),
                'status_code' => 404
            ],
            $throwable instanceof UnauthorizedHttpException => [
                'message' => $throwable->getMessage(),
                'status_code' => 403
            ],
            default => [
                'message' => $throwable->getMessage(),
                'status_code' => 500
            ],
        };

        $jsonResponse = new JsonResponse($response, $response["status_code"]);
        $event->setResponse($jsonResponse);

    }
}
